from flask import render_template, flash, redirect, url_for,request,jsonify,Response,make_response
from app import app, db
import json
import pprint
import json_tricks
import requests
import os,os.path
import errno
import sys
from pickle import dump,load

import pandas as pd
import numpy as np
from .analytics.preprocess import Preprocess


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def safe_open_w(path):
    mkdir_p(os.path.dirname(path))
    return open(path, 'wb')

def num(s):
    try:
        return float(s)
    except ValueError:
        return int(s)
def getInputOutputVariables(analyticsItem):
    try:
        x=[]
        y=[]
        y.append(analyticsItem['structure']['output_variables'])
        for input in analyticsItem['data']['collectionAttributes']:
            if input != y[0]:
                x.append(input)
        print("\ninput variables are: ",x)
        print("\noutput variables are: ",y)
        return (x,y)
    except Exception as e:
        str(e)
def getAnalyticsItem(analyticsName):
    try:
        collection="analyticsschemastructures"
        analytics = db[collection].find_one({"name":analyticsName})
        if analytics['user']:
            pass
        else:
            analytics['user']='johnDoe'
        if analytics:
            analyticsItem={
                "name": analytics['name'],
                "data": analytics['data'],
                "structure":analytics['structure'],
                #TODO
                "user":analytics['user']
                }
            return analyticsItem
        else:
            app.logger.error("Analytics Item Not found")
            raise "Analytics Item Not found"
    except Exception as e:
        str(e)
def setStatus(analyticsName,status):
    collection="analyticsschemastructures"
    db[collection].update_one(
        {"name": analyticsName},
        {
            "$set": {
                "status":status
            }
        }
    )
    analytics = db[collection].find_one({"name":analyticsName})
    print("status: ",analytics['status'])
    return
def setError(analyticsName,status,error):
    collection="analyticsschemastructures"
    db[collection].update_one(
        {"name": analyticsName},
        {
            "$set": {
                "status":status,
                "error":error
            }
        }
    )
    analytics = db[collection].find_one({"name":analyticsName})
    print("status: ",analytics['status'],"\n",error)
    return

def setResult(analyticsItem):
    collection="analyticsschemastructures"
    db[collection].update_one(
        {"name": analyticsItem['name']},
        {
            "$set": {
                "structure":analyticsItem['structure']
            }
        }
    )
    analytics = db[collection].find_one({"name":analyticsItem['name']})
    #app.logger.info("result: %s",analytics['structure'])
    return
def getDataFromCollection(analyticsItem):
    try:
        dataCollectionName=analyticsItem['data']['collectionName']
        dataRows=db[dataCollectionName].find()
        dataList = []
        dataAttributes=[]
        for attribute in analyticsItem['data']['collectionAttributes']:
            dataAttributes.append(attribute)
        for dataRow in dataRows:
            row=[]
            for attribute in dataAttributes:
                value=analyticsItem['data']['collectionAttributes'][attribute]
                if value == 'Number':
                    data_num=num(dataRow[attribute])
                else:
                    data_num=dataRow[attribute] 
                row.append(data_num)
            dataList.append(row)

        df=pd.DataFrame(dataList,columns=dataAttributes)
        types = df.dtypes
        #print(df.isnull().sum())
        #print(types)
        return df
    except Exception as e:
        str(e)
@app.route('/')
@app.route('/index')
def index():
    return "AAS STARTED"

@app.route('/analytics/train/<analyticsName>',methods=['GET', 'POST'])
def trainAnalyticalModel(analyticsName):
    try:
        app.logger.info('%s started', analyticsName)
        setStatus(analyticsName,"Started")
        analyticsItem=getAnalyticsItem(analyticsName)
        print(analyticsItem)
        df=getDataFromCollection(analyticsItem)
        print(df)
        
        # TODO get input variable as difference of create and add schema
        x,y = getInputOutputVariables(analyticsItem)
        p=Preprocess()       
        df=p.fill_missing(analyticsItem,df)
        df=p.label_encode(analyticsItem,df)
        #print(df)
    
        if 'mltask' in analyticsItem['structure']:
            x_data=pd.DataFrame(df, columns=x)
            y_data=pd.DataFrame(df, columns=y)
            from .analytics.mltask import Mltask
            ml=Mltask()
            analyticsItem['structure']['mltask']=ml.mltask(analyticsItem['structure']['mltask'],x_data,y_data,x,y)
            #print("printing result",results,names,'\n',analyticsItem)
           
            setResult(analyticsItem)
            setStatus(analyticsName,"Completed")
    except Exception as e:
        setError(analyticsName,"Error",str(e))
        return str(e),400
    return jsonify(analyticsItem)

@app.route('/analytics/finalTrain/<analyticsName>',methods=['GET', 'POST'])
def finalTrainAnalyticalModel(analyticsName):
    try:
        print(analyticsName)
        app.logger.info('%s final training started', analyticsName)
        setStatus(analyticsName,"Final training started")
        analyticsItem=getAnalyticsItem(analyticsName)
        user=analyticsItem['user']
        #print(analyticsItem)
        df=getDataFromCollection(analyticsItem)
        #print(df)

        x,y = getInputOutputVariables(analyticsItem)
        p=Preprocess()
        df=p.fill_missing(analyticsItem,df)
        from sklearn.preprocessing import LabelEncoder

        
        for col in df.columns:
            if df[col].dtype == np.object:
                le = LabelEncoder()
                df[col]=le.fit_transform(df[col])
                print('encode:',col,' ',le.classes_)
                filename = 'data/'+ user + '/' + analyticsItem['name'] + '_'+ col + '_le' +'.sav' 
                dump(le, safe_open_w(filename))
                #print(df[col])
        #p.preprocess(analyticsItem,df)
        
        if 'mltask' in analyticsItem['structure']:
            mltaskItem=analyticsItem['structure']['mltask']
            x_data=pd.DataFrame(df, columns=x)
            y_data=pd.DataFrame(df, columns=y)
            if 'Classification'== mltaskItem['task_type']:
                print("in ml classify")
                from app.analytics.classification import Classification
                model_cls=Classification()
            elif 'Regression' == mltaskItem['task_type']:
                print("in ml regression")
                from app.analytics.regression import Regression
                model_cls=Regression()
            # TODO get model
            from app.analytics.mltask import Mltask
            model=Mltask().getFinalModel(model_cls,analyticsItem['structure']['mltask'])     
            model.fit(x_data,y_data)
            filename = 'data/'+ user + '/' + analyticsItem['name'] +'.sav' 
            dump(model, open(filename, 'wb'))
            setStatus(analyticsName,"Final training completed")
    except Exception as e:
        setError(analyticsName,"Error",str(e))
        return str(e),400
    return jsonify(analyticsItem),200

@app.route('/analytics/test/<analyticsName>',methods=['GET', 'POST'])
def testAnalyticalModel(analyticsName):
    try: 
        query = request.get_json(silent=True, force=True)['inputs']
        input_df = pd.DataFrame(query)

        analyticsItem=getAnalyticsItem(analyticsName)
        user=analyticsItem['user']
        filename = 'data/'+ user +'/'+ analyticsName +'.sav'
        loaded_model = load(open(filename, 'rb'))
        app.logger.info(analyticsItem)
        x,y = getInputOutputVariables(analyticsItem)
        app.logger.info(x)
        x_test=pd.DataFrame(input_df, columns=x)
        print(x_test) 
        from sklearn.preprocessing import LabelEncoder
        for col in x_test.columns:
            if x_test[col].dtype == np.object:
                le_filename = 'data/'+ user + '/' + analyticsItem['name'] + '_'+ col + '_le' +'.sav'
                le = load(open(le_filename, 'rb')) 
                x_test[col]=le.transform(x_test[col])
                print(le.classes_)
        print(x_test)
        y_prediction = loaded_model.predict(x_test)
        print('predicted y is: ', y_prediction)

        le_filename = 'data/'+ user + '/' + analyticsItem['name'] + '_'+ str(y[0]) + '_le' +'.sav'
        le = load(open(le_filename, 'rb')) 
        print(le.classes_)
        y_prediction=le.inverse_transform(y_prediction)

        print('predicted y is: ', y_prediction)
        for inp,y_pred in zip(query,y_prediction):
            inp['prediction']=y_pred
    except Exception as e:
        setError(analyticsName,"Error",str(e))
        return str(e),400
    return json_tricks.dumps({"outputs":query})

@app.route('/analytics/visualize/bar/<schemaName>/<x>/<y>',methods=['GET', 'POST'])
def visualizeSchemaBar(schemaName,x,y):    
    collection="schemastructures"    
    analytics = db[collection].find_one({"name":schemaName})    
    if (x in analytics['structure']) and (y in analytics['structure']):
        names,values = getData(schemaName,analytics['structure'][x],analytics['structure'][y])        
        import matplotlib.pyplot as plt
        from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
        from io import BytesIO
        # data = {'apples': 'Xol', 'oranges': 'Mol', 'lemons': 'Xol', 'limes': 'Gol'}
        # names = list(data.keys())
        # values = list(data.values())
        # fig, axs = plt.subplots(1, 1, figsize=(9, 9), sharey=True)        
        fig, axs = plt.subplots(figsize=(14, 10))
        N = len(names)
        import numpy as np    
        colors = [ '#'+str(int(100000+i*100000)) for i in  np.random.rand(N)  ]
        print(colors)
        #area = np.pi * (15 * np.random.rand(N))**2  # 0 to 15 point radii
        wid = [i/N for i in range(0,N)]
        axs.bar(names, values,width=wid,alpha=0.15,color=colors)                
        # axs[1].scatter(names, values)
        # axs[2].plot(names, values)
        fig.suptitle('Visulalizing Attribute Relation')    
        plt.xlabel(x, fontsize=16)
        plt.ylabel(y, fontsize=16)    
        canvas=FigureCanvas(fig)
        png_output = BytesIO()
        canvas.print_png(png_output)        
        response=make_response(png_output.getvalue())
        response.headers['Content-Type'] = 'image/png'
        return response
    # resu = db["boston_dataset"].find({})
    # data_x = []
    # data_y = []
    # colors = []
    # for i in resu:
    #     data_x.append(i['CRIM'])        
    #     data_y.append(i['RM'])
    #     colors.append('red')
    # print(data_x)
    # # analyticsItem={
    # #     "name": analytics['name'],
    # #     "data": analytics['data'],
    # #     "structure":analytics['structure']
    # # }
    # # dataCollectionName=analyticsItem['data']['collectionName']
    # # dataRows=db[dataCollectionName].find()
    # # colormap = {'setosa': 'red', 'versicolor': 'green', 'virginica': 'blue'}
    # # colors = [colormap[x] for x in flowers['species']]

    # p = figure(title = "Iris Morphology")
    # p.xaxis.axis_label = 'Petal Length'
    # p.yaxis.axis_label = 'Petal Width'

    # p.circle(data_x, data_y,
    #         color=colors, fill_alpha=0.2, size=10)

    # output_file("iris.html", title="iris.py example")

    # show(p)
    return "404"

@app.route('/analytics/visualize/scatter/<schemaName>/<x>/<y>',methods=['GET', 'POST'])
def visualizeSchemaScatter(schemaName,x,y):    
    collection="schemastructures"    
    print("SOME")
    analytics = db[collection].find_one({"name":schemaName})        
    if (x in analytics['structure']) and (y in analytics['structure']):
        names,values = getData(schemaName,analytics['structure'][x],analytics['structure'][y])        
        import matplotlib.pyplot as plt
        from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
        from io import BytesIO
        # data = {'apples': 'Xol', 'oranges': 'Mol', 'lemons': 'Xol', 'limes': 'Gol'}
        #names = list(data.keys())
        #values = list(data.values())
        # fig, axs = plt.subplots(1, 1, figsize=(9, 9), sharey=True)        
        fig, axs = plt.subplots(figsize=(14, 10))  
        N = len(names)
        import numpy as np    
        colors = np.random.rand(N)  
        area = np.pi * (N*2 * np.random.rand(N))**2  # 0 to 15 point radii
        axs.scatter(names, values, s=area, c=colors, alpha=0.5, label="new")        
        # axs[1].scatter(names, values)
        # axs[2].plot(names, values)
        fig.suptitle('Visulalizing Attribute Relation')    
        plt.xlabel(x, fontsize=16)
        plt.ylabel(y, fontsize=16)    
        canvas=FigureCanvas(fig)
        png_output = BytesIO()
        canvas.print_png(png_output)        
        response=make_response(png_output.getvalue())
        response.headers['Content-Type'] = 'image/png'
        return response    
    return "404"

def getData(schemaName,x,y):
    data = db[schemaName+'s'].find()    
    dat={}
    x_data=[]
    y_data=[]    
    for i in (data):          
        # dat[i[x['name']]]=i[y['name']]    
        x_data.append(i[x['name']])
        y_data.append(i[y['name']])    
        print(i)    
    return x_data,y_data

@app.route('/addIris', methods=['GET', 'POST'])
def addIris():

    # Load dataset
    url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
    names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
    import pandas
    dataset = pandas.read_csv(url, names=names)
    array = dataset.values
    print(array)
    try:
        collection="iris"
        for row in array:
            db[collection].insert({'sepallength':row[0],'sepalwidth':row[1],'petallength':row[2],'petalwidth':row[3],'class':row[4]})
    except Exception as e:
        return str(e)        
    return "ADDED"