from flask import Flask
from config import Config
from pymongo import MongoClient
import json

app = Flask(__name__)
app.config.from_object(Config)

config_data = json.load(open('config.json'))
print(config_data["database"]["name"])
MONGO_DB = config_data["database"]["name"] 
client = MongoClient('mongodb://'+config_data["database"]["hostname"]+':'+str(config_data["database"]["port"])+'/')
db = client[MONGO_DB]

from app import routes

if __name__ == "__main__":
    app.run('0.0.0.0', debug=True)