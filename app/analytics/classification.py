# Load libraries
import pandas
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

class Classification:
    def __init__(self):
        print("Initialized Classification")
    def getModel(self,model_name,params):
        #print("\nsending model : ", model_name, " : ",params)
        if model_name=='K Neighbors Classifier':
            print("in KNN")
            model=self.getKNeighborsClassifier(params)
        elif model_name == "Decision Tree":
            print("in CART")
            model=self.getDecisionTreeClassifier(params)
        elif model_name == "Gaussian Naive Bayes Classifier":
            print("in Naive Bayes")
            model=self.getGaussianNB()
        elif model_name == "LDA":
            from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
            print("in LDA")
            model=LinearDiscriminantAnalysis(solver='svd', shrinkage=None, priors=None, n_components=None, store_covariance=False, tol=1e-4)
        return model

    def getDecisionTreeClassifier(self,params):
        try:
            #print(params)
            from sklearn.tree import DecisionTreeClassifier
            criterion =params['criterion']
            splitter =params['splitter']
            max_depth=None if (params['max_depth'] == '') else (int)(params['max_depth'])
            min_samples_split =(int)(params['min_samples_split'])
            min_samples_leaf =(int)(params['min_samples_leaf'])
            min_weight_fraction_leaf =(float)(params['min_weight_fraction_leaf'])    
            max_features = None if (params['max_features']=="") else (int)(params['max_features '])
            random_state =None if (params['random_state']=="") else (int)(params['random_state'])
            max_leaf_nodes =None if (params['max_leaf_nodes']=="") else (int)(params['max_leaf_nodes']) 
            class_weight =None if (params['class_weight']=="None") else params['class_weight'] 
            presort =False if (params['presort']=="False") else True 
            model=DecisionTreeClassifier(criterion=criterion, splitter=splitter, max_depth=max_depth,min_samples_split=min_samples_split,min_samples_leaf=min_samples_leaf,min_weight_fraction_leaf=min_weight_fraction_leaf,max_features=max_features,random_state=random_state,max_leaf_nodes=max_leaf_nodes,class_weight=class_weight,presort=presort)            
        except Exception as e:
            print("in decision tree except",str(e))
            model=DecisionTreeClassifier(criterion="gini", splitter="best", max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0., max_features=None, random_state=None, max_leaf_nodes=None, class_weight=None, presort=False)            
        return model

    def getKNeighborsClassifier(self,params):
        from sklearn.neighbors import KNeighborsClassifier
        try:
            n_neighbors =(int)(params['n_neighbors'])
            weights =params['weights']
            algorithm =params['algorithm']
            leaf_size =(int)(params['leaf_size'])
            metric =params['metric']
            n_jobs =(int)(params['n_jobs'])

            model=KNeighborsClassifier(n_neighbors=n_neighbors, weights=weights, algorithm=algorithm, leaf_size=leaf_size, metric=metric, n_jobs=n_jobs)
        except Exception as e:
            print("in KNN except\n",str(e))
            model=KNeighborsClassifier(n_neighbors=5, weights='uniform', algorithm='auto', leaf_size=30, p=2, metric='minkowski', metric_params=None, n_jobs=1)      
        return model

    def getGaussianNB(self):
        from sklearn.naive_bayes import GaussianNB
        model=GaussianNB()
        return model