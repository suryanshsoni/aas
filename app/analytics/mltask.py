# Load libraries
import pandas
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
import re
import json_tricks
class Mltask:
    def __init__(self):
        print("Initialized MLTASK")
    def mltask(self,mltaskItem,x_data,y_data,x,y):
        print("in mltask")
        
        # Test options and evaluation metric
        seed = 7
        if 'Neg Log Loss' == mltaskItem['scoring']:
            scoring='neg_log_loss'
        elif 'ROC AUC' == mltaskItem['scoring']:
            scoring='roc_auc'
        elif 'Accuracy' == mltaskItem['scoring']:  
            scoring = 'accuracy'
        elif 'R2' == mltaskItem['scoring']:  
            scoring = 'r2'
        elif 'Neg Mean Absolute Error' == mltaskItem['scoring']:  
            scoring = 'neg_mean_absolute_error'
        elif 'Neg Mean Squared Error' == mltaskItem['scoring']:  
            scoring = 'neg_mean_squared_error'
        else:
            scoring='r2'
        # Spot Check Algorithms
        models = []
        results=[]
        names=[]

        evaluate = mltaskItem['eval']
        appendModel=None
        appendModel=mltaskItem['algoParams']
        #print("Append Model",appendModel)
        model_cls=None
        if 'Classification'== mltaskItem['task_type']:
            from app.analytics.classification import Classification
            model_cls=Classification()
            print('doing classification')
        elif 'Regression' == mltaskItem['task_type']:
            #appendModel=mltaskItem['regression']
            from app.analytics.regression import Regression
            model_cls=Regression()
            print('doing regression')
        for algo in appendModel:
            #print("\n",algo,"\n")
            models.append((algo['chosenValue'],model_cls.getModel(algo['chosenValue'],algo['params'])))
        #print('before evaluation',models,appendModel)
        if 'Test and Train' == evaluate['chosenValue']:
            split=(float)(evaluate['params']['fraction'])
            print('in test train',split)
            from sklearn.model_selection import train_test_split 
            X_train, X_test, Y_train, Y_test = train_test_split(x_data.values, y_data.values[:,0], test_size=1-split, random_state=7)
            iter=0
            for name, model in models:
                model.fit(X_train,Y_train)
                result = model.score(X_test, Y_test)
                #results.append(result)
                #names.append(name)
                #msg = "%s: %f" % (name, result*100.0)
                #print(msg)
                appendModel[iter]['result']={}
                if 'Classification'== mltaskItem['task_type']:
                    y_pred=model.predict(X_test)
                    conf_mat=confusion_matrix(Y_test,y_pred)
                    report = classification_report(Y_test, y_pred)
                    #print(conf_mat,report,appendModel[iter])

                    #appendModel[iter]['result']['report']=json_tricks.dumps(report)
                    appendModel[iter]['result']['conf_matrix']=str(conf_mat).replace("\n",",")
                    report=str(report).replace("\n","<br>")
                    x = []
                    report = re.sub(' +',' ',report)
                    report = report.replace('avg / total','Avg/Total')
                    report = report.replace('<br>','\n')
                    report = re.sub('\n+','\n',report)
                    for i in report.split('\n'):
                        if(len(i.strip().split()) != 0):
                            x.append(i.strip().split())
                    appendModel[iter]['result']['report']=x[1:]
                appendModel[iter]['result']['mean']=round(result.mean(),3)
                appendModel[iter]['result']['std']=round(result.std(),3)
                iter+=1
        elif 'K-Fold' == evaluate['chosenValue']:
            from sklearn.model_selection import KFold 
            from sklearn.model_selection import cross_val_score, cross_val_predict
            print("in kfold")
            folds=(int)(evaluate['params']['fold_count'])
            kfold=None
            kfold = KFold(n_splits=folds, random_state=seed)
            iter=0
            for name, model in models:
                cv_results = cross_val_score(model,x_data.values, y_data.values[:,0], scoring=scoring,cv=kfold)
                results.append(cv_results)
                names.append(name)
                #msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())    
                #print(msg)
                appendModel[iter]['result']={}
                if 'Classification'== mltaskItem['task_type']:
                    y_pred= cross_val_predict(model,x_data.values, y_data.values[:,0],cv=kfold)
                    conf_mat = confusion_matrix(y_data,y_pred)
                    report = classification_report(y_data, y_pred)
                    print(conf_mat,report,appendModel[iter])
                    #appendModel[iter]['result']['report']=str(report).replace("\n","<br>")
                    temp=str(conf_mat).replace("\n",",")
                    appendModel[iter]['result']['conf_matrix']=re.sub('\d+\ ','\d+,',temp)
                    report=str(report).replace("\n","<br>")
                    x = []
                    report = re.sub(' +',' ',report)
                    report = report.replace('avg / total','Avg/Total')
                    report = report.replace('<br>','\n')
                    report = re.sub('\n+','\n',report)
                    for i in report.split('\n'):
                        if(len(i.strip().split()) != 0):
                            x.append(i.strip().split())
                    appendModel[iter]['result']['report']=x[1:]
                appendModel[iter]['result']['mean']=round(cv_results.mean(),3)
                appendModel[iter]['result']['std']=round(cv_results.std(),3)
                iter+=1
        return mltaskItem
    def getFinalModel(self,model_cls,mltaskItem):
        algos=mltaskItem['algoParams']
        for algo in algos:
            if 'final' in algo and algo['final']=="true":
                break
        model=model_cls.getModel(algo['chosenValue'],algo['params'])
        print("\n\nFinal Model is: ",algo['chosenValue'])
        return model