# Load libraries
import pandas
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

class Preprocess:
    def __init__(self):
        print("Initialized Preprocess")
    def label_encode(self,analyticsItem,df):
        print("in label encode")
        from sklearn.preprocessing import LabelEncoder
        import numpy as np
        for col in df.columns:
            if df[col].dtype == np.object:
                le = LabelEncoder()
                df[col]=le.fit_transform(df[col])
                #print('encode:',col,' ',le.classes_)
                #print(df[col])
        return df
    
    def fill_missing(self,analyticsItem,df):
        print("in fill missing\n")
        try:
            if 'numerical' in analyticsItem['structure']['preprocessing']:
                numerical=analyticsItem['structure']['preprocessing']['numerical']
                #print(numerical)
                for col in numerical:
                    chosenValue=numerical[col]['chosenValue']
                    params=numerical[col]['params']
                    if chosenValue=='Fill By Values':
                        if params['fill_by']=='Mean':
                            #print(col,":",chosenValue,":",params)
                            df[col].fillna(df[col].mean(), inplace=True)
                        elif params['fill_by']=='Median':
                            #print(col,":",chosenValue,":",params)
                            df[col].fillna(df[col].median(), inplace=True)
                        elif params['fill_by']=='Zero':
                            #print(col,":",chosenValue,":",params)
                            df[col].fillna(value=0, inplace=True)
                        elif params['fill_by']=='Forward':
                            #print(col,":",chosenValue,":",params)
                            df[col].fillna(method='ffill',inplace=True)
                        elif params['fill_by']=='Backward':
                            #print(col,":",chosenValue,":",params)
                            df[col].fillna(method='bfill',inplace=True)
                    elif chosenValue=='Drop Row':
                        #print(col,":",chosenValue,":",params)
                        df.dropna(subset=[col], how='any')
                        pass
                    #df["preTestScore"].fillna(df["preTestScore"].mean(), inplace=True)
            if 'categorical' in analyticsItem['structure']['preprocessing']:
                categorical=analyticsItem['structure']['preprocessing']['categorical']
                #print(categorical)
                for col in categorical:
                    chosenValue=categorical[col]['chosenValue']
                    params=categorical[col]['params']
                    if chosenValue=='Use Default':
                        #print(col,":",chosenValue)
                        #ufo['Shape Reported'].fillna(value='VARIOUS', inplace=True)
                        df[col].fillna(value=params['default_value'],inplace=True)
                        pass
                    elif chosenValue=='Drop Row':
                        #print(col,":",chosenValue)
                        #ufo.dropna(subset=['City', 'Shape Reported'], how='any')
                        df.dropna(subset=[col], how='any')
                        pass
                    #df["preTestScore"].fillna(df["preTestScore"].mean(), inplace=True)
        except Exception as e:
            print(str(e))
        return df