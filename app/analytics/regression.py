class Regression:
    def __init__(self):
        print("Initialized Regression")
    def getModel(self,model_name,params):
        model=None
        if model_name=='Linear Regression':
            print("in LinearRegreesion")
            model=self.getLinearRegression(params)
        elif model_name=='Logistic Regression':
            print("in Logistic Regression")
            model=self.getLogisticRegression(params)
        elif model_name=='SVR':
            print("in SVR")
            model=self.getSVR(params)
        '''elif model_name=='Ridge':
            print("in Ridge")
            from sklearn.linear_model import Ridge
            if params['default'] == True:
                print("in Ridge default")
                from sklearn.linear_model import Ridge
                model=Ridge()
        elif model_name=='Lasso':
            print("in Lasso")
            if params['default'] == True:
                print("in Lasso default")
                from sklearn.linear_model import Lasso
                model=Lasso()
        elif model_name=='KNeighborsRegressor':
            print("in KNeighborsRegressor")
            if params['default'] == True:
                print("in KNeighborsRegressor default")
                from sklearn.neighbors import KNeighborsRegressor
                model=KNeighborsRegressor()
        elif model_name=='DecisionTreeRegressor':
            print("in DecisionTreeRegressor ")
            if params['default'] == True:
                print("in DecisionTreeRegressor default")
                from sklearn.tree import DecisionTreeRegressor 
                model=DecisionTreeRegressor(criterion="mse", splitter="best", max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0., max_features=None, random_state=None, max_leaf_nodes=None, min_impurity_split=1e-7, presort=False)
        '''
        return model
    def getLinearRegression(self,params):
        from sklearn.linear_model import LinearRegression
        try:
            fit_intercept =True if (params['fit_intercept']=="True") else False
            normalize =False if (params['normalize']=="False") else True
            copy_x =True if (params['copy_x']=="True") else False
            n_jobs=(int)(params['n_jobs'])
            model=LinearRegression(fit_intercept=fit_intercept, normalize=normalize, copy_X=copy_x, n_jobs=n_jobs)
        except Exception as e:
            print("in Linear Regression except\n",str(e))
            model=LinearRegression(fit_intercept=True, normalize=False, copy_X=True, n_jobs=1)
        return model

    def getLogisticRegression(self,params):
        from sklearn.linear_model import LogisticRegression
        try:
            penalty =params['penalty']
            fit_intercept =True if (params['fit_intercept']=="True") else False
            n_jobs=(int)(params['n_jobs'])
            tol =(float)(params['tol'])
            C =(float)(params['C'])
            intercept_scaling =(float)(params['intercept_scaling'])
            max_iter=(int)(params['max_iter'])
            dual =False if (params['dual']=="False") else True
            warm_start =False if (params['warm_start']=="False") else True
            class_weight=None if (params['class_weight'] == 'None') else (params['class_weight'])
            random_state=None if (params['random_state'] == '') else (int)(params['random_state'])
            solver =params['solver']
            multi_class =params['multi_class']

            model=LogisticRegression(penalty=penalty, dual=dual, tol=tol, C=C, fit_intercept=fit_intercept, intercept_scaling=intercept_scaling, class_weight=class_weight, random_state=random_state, solver=solver, max_iter=max_iter, multi_class=multi_class, warm_start=warm_start, n_jobs=n_jobs)
        except Exception as e:
            print("in Logistic Regression except\n",str(e))
            model=LogisticRegression(penalty='l2', dual=False, tol=1e-4, C=1.0, fit_intercept=True, intercept_scaling=1, class_weight=None, random_state=None, solver='liblinear', max_iter=100, multi_class='ovr', verbose=0, warm_start=False, n_jobs=1)
        return model
    
    def getSVR(self,params):
        from sklearn.svm import SVR
        try:
            #"cache_size": 1,
            
            kernel =params['kernel']
            degree =(int)(params['degree'])
            gamma="auto" if (params['gamma'] == '') else (float)(params['gamma'])
            C =(float)(params['C'])
            coef0 =(float)(params['coef0'])
            tol =(float)(params['tol'])
            epsilon =(float)(params['epsilon'])
            shrinking =True if (params['shrinking']=="True") else False
            max_iter=(int)(params['max_iter'])
            model=SVR(kernel=kernel, degree=degree, gamma=gamma, coef0=coef0, tol=tol, C=C, epsilon=epsilon, shrinking=shrinking,max_iter=max_iter)
        except Exception as e:
            print("in SVR except\n",str(e))
            model=SVR(kernel='rbf', degree=3, gamma='auto', coef0=0.0, tol=1e-3, C=1.0, epsilon=0.1, shrinking=True, cache_size=200, verbose=False, max_iter=-1)
        return model